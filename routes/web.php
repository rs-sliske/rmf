<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('test', function(){
	\Artisan::call('help');
	return 'hello world';
});

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', ['uses' => 'HomeController@index', 'as' => 'home']);

Route::group(['prefix' => 'community'], function(){
	Route::resource('topic', 'TopicController');
	Route::resource('post', 'PostController');

	Route::post('topic/{topic}', ['uses' => 'TopicController@reply', 'as'=>'community.topic.reply']);
	Route::post('topic/{topic}', ['uses' => 'TopicController@bump', 'as'=>'community.topic.bump']);

	Route::get('/', 'TopicController@index');



});

Route::get('verify/email/{code}', ['as' => 'email.confirm', 'uses' => 'EmailController@confirm']);


Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function(){
	Route::get('alternate', ['uses' => 'LoginController@getAlternate']);
	Route::post('alternate', ['uses' => 'LoginController@postAlternate']);

	Route::get('2fa/setup', ['uses'=>'TwoStepController@setup']);
	Route::post('2fa/setup', ['uses'=>'TwoStepController@checkSetup']);
	Route::get('2fa', ['uses'=>'TwoStepController@show']);
	Route::post('2fa', ['uses'=>'TwoStepController@check']);

	Route::get('magic/{user?}', ['as' => 'auth.magic.send', 'uses' => 'LoginController@sendMagicLink']);
	Route::get('magic/{user}/{code}', ['as' => 'auth.magic.check', 'uses' => 'LoginController@checkMagicLink']);

	Route::get('oauth', function(){
		return view('auth.oauth');
	})->middleware('auth');
});

Route::resource('user', 'UserController');
Route::get('profile', function(){
	return redirect()->route('user.show', Auth::user()->slug());
})->middleware('auth');
