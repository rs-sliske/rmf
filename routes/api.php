<?php 

Route::group(['prefix' => 'v1', 'namespace' => 'v1'], function(){
	$names = [ 'names' => ['index' => '', 'create' => '', 'store' => '', 'show' => '', 'edit' => '', 'update' => '', 'destroy' => '']];
	Route::get('users/~me~/oauth', ['uses' => 'UserController@oauth']);
	Route::resource('users', 'UserController', $names);

	Route::group(['prefix' => 'community'], function() use($names){
		Route::resource('post', 'PostController', $names);
		Route::resource('topic', 'TopicController', $names);
	});


	Route::get('test', function(){
		return response()->json(\Auth::user()->format());
	})->middleware('auth:api');
});