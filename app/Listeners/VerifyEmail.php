<?php

namespace RMF\Listeners;

use RMF\Events\EmailAddressAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use RMF\Models\EmailVerificationCode;

use Mail;

class VerifyEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailAddressAdded  $event
     * @return void
     */
    public function handle(EmailAddressAdded $event)
    {
        // $event->email()->sendConfirmationEmail();        
    }
}
