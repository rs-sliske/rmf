<?php

namespace RMF\Listeners;

use RMF\Events\PasswordChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class SendPasswordChangedWarningEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PasswordChanged  $event
     * @return void
     */
    public function handle(PasswordChanged $event)
    {
        $user = $event->user();
        Mail::send('emails.passwordChanged',['user' => $user], function($message) use ($user) {
            $message->from('warning@rmf.dev', 'RMF DEV');
            $message->to($user->emails[0]->email);
            $message->subject('Your password has been changed');
        });
    }
}
