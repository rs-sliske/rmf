<?php

namespace RMF\Models;

use RMoore\ChangeRecorder\RecordsChanges;
use Illuminate\Database\Eloquent\SoftDeletes;


use RMF\Traits\Permissible;

class UserGroup extends Model
{
	use RecordsChanges, SoftDeletes, Permissible;

    protected $fillable = ['name'];

	public static function make($name, $priority = 1){
		$group = new static;

		$group->name = $name;
		$group->priority = $priority;

		$group->save();
		return $group;
	}	

	public static function findByName($name){
		return static::where('name', $name)->first();
	}

	
}
