<?php

namespace RMF\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use RMoore\ChangeRecorder\RecordsChanges;

class Post extends Model
{
    use RecordsChanges, SoftDeletes;

    protected $fillable = ['content'];

    protected $touches = ['topic'];

    public static function boot(){
        static::creating(function($post){
            $post->topic->touchChanged();            
        });

        static::updating(function($post){
            if(array_key_exists('content', $post->getDirty())){                
                $post->topic->touchChanged();
            }
        });
    }

    public function author(){
    	return $this->belongsTo(User::class, 'author_id');
    }

    public function topic(){
    	return $this->belongsTo(Topic::class);
    }

    public function format(){
        return [
            'author' => $this->author->name(),
            'created_at' => $this->created_at->timestamp,
        ];
    }
}
