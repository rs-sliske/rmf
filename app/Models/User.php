<?php

namespace RMF\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use RMoore\ChangeRecorder\RecordsChanges;
use Illuminate\Database\Eloquent\SoftDeletes;

use RMF\Events\UserRegistered;
use RMF\Events\PasswordChanged;

use Illuminate\Notifications\Notifiable;

use Hash;
use Event;
use DB;
use Carbon\Carbon;

use RMF\Traits\Slugable;
use RMF\Traits\Permissible;
use RMF\Traits\BelongsToRank;
use RMF\Traits\Findable;

use Laravel\Passport\HasApiTokens;

use RMoore\SocialiteExtension\Models\SocialSite;
use RMoore\SocialiteExtension\Models\SocialLogin;

class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable,    Authorizable,   CanResetPassword, 
        SoftDeletes,        Slugable,       Permissible,
        RecordsChanges,     Findable,       BelongsToRank,
        Notifiable,         HasApiTokens;


    protected $fillable = [
        'username', 'password', 'name', 'display_name'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'magic_login_expires_at'
    ];

    protected $casts = [
        'google2fa_confirmed' => 'boolean',
    ];

    public function getRouteKeyName(){
        return 'display_name';
    }

    public function routeNotificationForMail(){
        return $this->emails->first()->email;
    }

    public function avatar(){
        $default = "https://placehold.it/150x150";
        $size = 150;
        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $this->email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
    }  

    public function name(){
        return $this->display_name;
    }

    public function slugEnd(){
        return $this->name();
    }

    public function changePassword(string $password){
        $this->password = Hash::make($password);
        $this->save();
        Event::fire(new PasswordChanged($this));
    }

    public function changeName(string $displayName){
        $this->display_name = $displayName;
        $this->save();
    }

    public function remove2fa(){
        $this->google2fa_confirmed = false;
        $this->google2fa_secret = null;
        $this->save();
    }

    public function posts(){
        return $this->hasMany(Post::class, 'author_id');
    }

    public function emails(){
        return $this->hasMany(EmailAddress::class);
    }

    public function socialLogins(){
        return $this->hasMany(SocialLogin::class);
    }

    public function connectedSocialSites(){
        return $this->belongsToMany(SocialSite::class, 'social_logins', 'user_id', 'provider_id')->withTimestamps()->withPivot('social_site_id')->active();
    }


    public function checkPermission($permission, $minmax = 'max', $includeGroups = true){
        if(! $permission instanceof Permission)
            $permission = Permission::findByName($permission);
        
        $failResult = [
            'exists' => false,
            'on' => null,
            'value' => null,
            'model' => null,
        ];

        if(!$permission)
            return $failResult;

        $u = $this->permissions()->where('permission_id', $permission->id)->orderBy('pivot_permission_value', ($minmax == 'min' ? 'ASC' : 'DESC') )->first();
        if($u){
            return [
                'exists' => true,
                'on' => User::class,
                'value' => $u->pivot->permission_value,
                'model' => $u,
            ];
        }
        if($includeGroups)
            foreach($this->activeGroups as $group){
                $r = $group->checkPermission($permission, $minmax);

                if($r['exists'])
                    return $r;
            }

        return $failResult;
    }

    public function toArray(){
        return [
            'id' => $this->id,
            'name' => $this->name(),
            'username' =>  $this->username,
            'email' => $this->emails->first()->email,
            'ranks' => $this->activeGroups->pluck('name'),
            'avatar_url' => $this->avatar(),
        ];
    }   

    public static function findByName(string $name){
        return static::findByDisplayName($name);
    }

    public static function register($credentials){
        $user = new static;
        DB::transaction(function () use ($user, $credentials){ 
            $user->email = $credentials['email'];
            $user->username = $credentials['username'];
            $user->display_name = $credentials['username'];
            $user->password = array_key_exists('password', $credentials) ? Hash::make($credentials['password']) : '';

            $user->save();

            $user->assignRank('validating');

            EmailAddress::make($credentials['email'], $user, false);

            Event::fire(new UserRegistered($user));
            
        });
        return $user;
    }


}
