<?php

namespace RMF\Models;

use Illuminate\Support\Str;
use Carbon\Carbon;

class EmailVerificationCode extends Model
{
    protected $table = 'email_codes';

    protected $dates = ['expires_at'];

    public function getRouteKeyName(){
        return 'code';
    }

    public static function make(EmailAddress $email){
        $code = new static;
        $code->email_id = $email->id;
        $code->code = Str::random(60);
        $code->expires_at = Carbon::now()->addHour();
        $code->save();
        return $code;
    }

    public function email(){
        return $this->belongsTo(EmailAddress::class, 'email_id');
    }
}
