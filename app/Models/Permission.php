<?php

namespace RMF\Models;

use RMoore\ChangeRecorder\RecordsChanges;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
	use SoftDeletes;

    protected $fillable = ['name'];

    public static function findByName($name){
		return static::where('name', $name)->first();
	}
}
