<?php

namespace RMF\Models;

use RMF\Events\EmailAddressAdded;
use DB;
use Event;
use Carbon\Carbon;
use Mail;


class EmailAddress extends Model
{
	protected $dates = ['confirmed_at'];

    public static function make(string $email, User $user, $fireEvent = true){
    	$address = new static;

        DB::transaction(function () use ($address, $email, $user){ 
            $address->email = $email;
	    	$address->user_id = $user->id;

            $address->save();
        });

        if($fireEvent)
            Event::fire(new EmailAddressAdded($address));

        return $address;
    }

    public function confirm(){
    	$this->confirmed_at = Carbon::now();
    	$this->save();

        $this->user->ranks()->attach(UserGroup::findByName('member'));
        $this->user->ranks()->detach(UserGroup::findByName('validating'));
    }

    public function sendConfirmationEmail(){
    	$address = $this->email;
    	$code = EmailVerificationCode::make($this);

        Mail::send('emails.verifyEmail',['code' => $code], function($message) use ($address) {
            $message->from('confirm@rmf.dev', 'RMF DEV');
            $message->to($address);
            $message->subject('Verify your email');
        });
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
