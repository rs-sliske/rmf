<?php

namespace RMF\Models;

class Model extends \Illuminate\Database\Eloquent\Model
{
    public function format(){
    	return $this->toArray();
    }

    public function formatCollection($collection){
    	$res = [];

    	foreach($collection as $model){
    		$res[] = $model->format();
    	}

    	return $res;
    }
}
