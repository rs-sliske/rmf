<?php

namespace RMF\Models;

use RMoore\ChangeRecorder\RecordsChanges;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use RMF\Traits\Slugable;
use Auth;

class Topic extends Model
{
    use RecordsChanges, SoftDeletes, Slugable;

    protected $fillable = ['section_id', 'title', 'author_id'];

    protected $dates = ['deleted_at', 'bumped_at', 'changed_at'];

    public function posts(){
    	return $this->hasMany(Post::class)->orderBy('created_at');
    }

    public function slugEnd(){
        return $this->title;
    }

    public function lastReadAt(){
        $r = $this->views->where('pivot.user_id', 1)->sortByDesc('pivot.created_at')->first();
        return $r ?  $r->pivot['created_at'] : $this->created_at->subDay();
    }

    public function hasBeenRead(){
        return $this->lastReadAt()->gt($this->changed_at);
    }

    public function views(){
        return $this->belongsToMany(User::class, 'topic_views')->withTimestamps();
    }

    public function viewCount(){
        return $this->view_count;
    }

    public function lastPoster(){
        return $this->posts->last()->author;
    }

    public function reply($args){
    	$post = new Post;
    	$post->topic_id = $this->id;
    	$post->author_id = \Auth::id() ?? 0;
    	$post->content = $args['content'];
    	$post->save();

        $this->bumped_at = Carbon::now();
        $this->save();

    	return $post;
    }

    public function touchChanged(){
        $this->changed_at = $this->freshTimestamp();
        $this->save();
    }
}
