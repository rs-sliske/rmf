<?php

namespace RMF\Http\Middleware;

use Closure;

class CheckUserBelongsToGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $group)
    {
        $user = $request->user();

        if(! $user->isAMemberOf($group)){
            alert()->danger('you dont have permission to do this');
            return redirect('/')->withError('you dont have permission to do this');
        }

        return $next($request);
    }
}
