<?php

namespace RMF\Http\Middleware;

use Closure;

class UserHasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission, int $value = -1, $check = '>')
    {
        $user = $request->user();
        if($user){
            $res = $user->checkPermission($permission);

            if($res['exists']){
                if($this->checkValue($res['value'], $check, $value)){
                    return $next($request);
                }
            }
        }
        return redirect()->intended('/')->with('error[]', 'you do not have correct permissions for this');        
    }

    private function checkValue($value, $check, $arg){
        switch($check){
            case '>':
                return $value >= $arg;
            case '<':
                return $value <= $arg;
            case '=':
                return $value == $arg;
        }
        return false;
    }
}
