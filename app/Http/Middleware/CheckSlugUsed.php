<?php

namespace RMF\Http\Middleware;

use Closure;
use URL;

class CheckSlugUsed
{

    protected static $keyMap = [
        'user' => \RMF\Models\User::class,
        'topic' => \RMF\Models\Topic::class,
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $key = null)
    {
        $params = $request->route()->parameters();

        $value = null;

        if($key){
            if(array_key_exists($key, $params))
                $value = $params[$key];
        } else if (count($params) == 1){
            $value = array_values($params)[0];
            $key = array_keys($params)[0];
        }

        if($value && array_key_exists($key, static::$keyMap)){
            $model = static::$keyMap[$key]::findBySlug($value);

            if($value != $model->slug()){
                return redirect()->to(URL::toRoute($request->route(), [$key => $model->slug()]));            
            }
        }

        return $next($request);
    }
}
