<?php

namespace RMF\Http\Controllers\api;

use Illuminate\Http\Request;

use RMF\Http\Requests;
use RMF\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;

class ApiController extends Controller
{
   
    protected function formatCollection(Collection $data){
    	$res = [];
    	foreach($data as $obj)
    		$res[] = $obj->format();
    	
    	return $res;
    }
}
