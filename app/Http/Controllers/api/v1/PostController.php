<?php

namespace RMF\Http\Controllers\api\v1;

use Illuminate\Http\Request;

use RMF\Http\Requests;
use RMF\Http\Controllers\api\ApiController;

use RMF\Models\Post;
use Auth;

class PostController extends ApiController
{
    public function __construct(){
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return response()->json($post->format());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if(Auth::guard('api')->id() == $post->author_id){
            $post->delete();
            return response()->json([
                'status' => 'ok',
            ],200);
        }
        return response()->json([
            'status' => 'failed',
        ],400);

    }
}
