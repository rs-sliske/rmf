<?php

namespace RMF\Http\Controllers\Auth;

use Illuminate\Http\Request;

use RMF\Http\Requests;
use RMF\Http\Controllers\Controller;

use RMF\Models\User;
use Google2FA;
use Session;
use Auth;

class TwoStepController extends Controller
{
    public function setup(){
    	$user = Auth::user();
    	if(!$user->google2fa_secret){
	    	$user->google2fa_secret = Google2FA::generateSecretKey();
	    	$user->save();
	    }

    	if($user->google2fa_secret && $user->google2fa_confirmed){
    		session()->flash('error', 'you already have 2 step verification set up');
    		return redirect()->back();
    	}

    	$google2fa_url = Google2FA::getQRCodeGoogleUrl(
			'RMF Dev',
			$user->email,
			$user->google2fa_secret
		);

		return view('auth.2fa-setup')->with('google2fa_url', $google2fa_url);
    }

    public function checkSetup(Request $request){
    	$secret = $request->input('secret');
    	$user = Auth::user();

    	if(Google2FA::verifyKey($user->google2fa_secret, $secret)){
    		$user->google2fa_confirmed = true;
    		$user->save();
	    	return redirect()->intended('/')->with('success','successfully set up 2 step verification');
    	}

    	return redirect()->back()->with('error', 'code did not match, please try again');	   
    }

    public function show(){    	
		return view('auth.2fa');
    }

    public function check(Request $request){
    	$secret = $request->input('secret');

    	$user = User::findOrFail(Session::get('user_id'));

    	if(Google2FA::verifyKey($user->google2fa_secret, $secret)){
    		Auth::login($user);
    		Session::forget('user_id');
	    	return redirect()->intended('/');
    	}

    	return redirect()->back()->with('error', 'code did not match, please try again');	   
    }
}
