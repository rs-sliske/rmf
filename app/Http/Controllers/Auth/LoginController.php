<?php
namespace RMF\Http\Controllers\Auth;

use RMF\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use RMF\Models\User;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;

use Session;
use Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;
use Mail;

use RMF\Notifications\User\MagicSignIn;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request){
        $this->validate($request, ['username' => 'required']);

        $name = $request->input('username');

        $user = User::where('username', $name)->firstOrFail();

        $password = $request->input('password');

        if(Hash::check($password, $user->password)){
            if($user->google2fa_confirmed){
                Session::put('user_id', $user->id);
                return redirect('/auth/2fa');
            }
            Auth::login($user);
            return redirect()->intended('/');
        }
        Session::put('user_id', $user->id);
        return redirect('auth/alternate');
    }

    public function getAlternate(){
        return view('auth.alternate')->withUser(User::findOrFail(Session::get('user_id')));
    }

    public function postAlternate(Request $request){
        $user = User::findOrFail(Session::get('user_id'));

        $password = $request->input('password');

        if(Hash::check($password, $user->password)){
            if($user->google2fa_confirmed){
                return redirect('/auth/2fa');
            }
            Session::forget('user_id');
            Auth::login($user);
            return redirect()->intended('/');
        }

        return redirect()->back();
    }

    public function sendMagicLink(User $user = null){
        if(!$user || !$user->exists){
            $user = User::find(Session::get('user_id'));
        }
        if(!$user || !$user->exists){
            session()->flash('error', 'no user found');
            return redirect()->back();
        }

        $user->magic_login_key = Str::random(60);
        $user->magic_login_expires_at = Carbon::now()->addHour();
        $user->save();

        $user->notify(new MagicSignIn($user));

        return view('auth.magic-link-sent', ['user' => $user]);
    }

    public function checkMagicLink(User $user, $code){
        if($user->magic_login_key != $code){
            session()->flash('error', 'code does not match');
            return redirect('/');
        }

        if(Carbon::now()->gt($user->magic_login_expires_at)){
            session()->flash('error', 'that code has expired');
            return redirect('/');
        }

        $user->magic_login_key = null;
        $user->magic_login_expires_at = null;
        $user->save();

        if($user->google2fa_confirmed){
            Session::put('user_id', $user->id);
            return redirect('/auth/2fa');
        }
        Auth::login($user);

        session()->flash('success', 'welcome back');
        return redirect('/');
        
    }
}