<?php

namespace RMF\Http\Controllers;

use Illuminate\Http\Request;

use RMF\Http\Requests;

use RMF\Models\Topic;

use Auth;

use Carbon\Carbon;

class TopicController extends Controller
{
    public function __construct(){
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->middleware('slug:topic');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::latest('bumped_at')->with('posts', 'posts.author', 'posts.author.ranks', 'views')->get();
        return view('topic.list')->withTopics($topics)->with('useJS', false);//! $request->exists('nojs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('topic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $topic = new Topic;
        // $topic->user_id = Auth::id();
        $topic->title = $request->input('title');
        $topic->save();

        $topic->reply(['content' => $request->input('content')]);

        return redirect()->route('topic.show', ['topic' => $topic->slug()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug, Request $request)
    {
        $topic = Topic::fromSlug($slug)->with('posts', 'posts.author')->firstOrFail();
        $topic->increment('view_count');
        // if(Auth::check())
            $topic->views()->attach(Auth::id()?:0);
        
        return view('topic.show')->withTopic($topic)->with('useJS', $request->exists('nojs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reply($slug, Request $request){
        $topic = Topic::findFromSlug($slug);

        $topic->reply($request->all());

        return redirect()->route('topic.show', ['topic' => $topic->slug()]);
    }

    public function bump($slug){
        $topic = Topic::findFromSlug($slug);
        $topic->bumped_at = Carbon::now();
        $topic->save();
        return redirect()->back()->with('success', "bumped '$topic->title'");
    }
}
