<?php

namespace RMF\Http\Controllers;

use Illuminate\Http\Request;

use RMF\Http\Requests;
use RMF\Models\EmailVerificationCode;
use Auth;

class EmailController extends Controller
{
	public function __construct(){
		$this->middleware('auth');	
	}

    public function confirm($code){
    	$c = EmailVerificationCode::whereCode($code)->with('email', 'email.user')->first();
    	if(! $c){
    		return redirect()->route('home')->with('error', 'invalid code');
    	}
    	$email = $c->email;
    	if($email->user_id != Auth::id()){
    		return redirect()->route('home')->with('error', 'could not confirm email');
    	}
    	$email->confirm();
    	$c->delete();
    	return redirect()->route('home')->with('success', 'email address confirmed');
    }
}
