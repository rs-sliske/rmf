<?php 

namespace RMF\Traits;

trait Findable {
	public static function __callStatic($method, $parameters){
        $m = static::_findableMagic($method, $parameters);

        return $m ?: parent::__callStatic($method, $parameters);
    }

    protected static function _findableMagic($method, $parameters){
        $matches = [];
        if(preg_match('/(?<=(?:findBy))(.+)/', $method, $matches)){ 
            $attr = snake_case($matches[0]);
            return parent::where($attr, $parameters[0])->firstOrFail();
        }

        return null;
    }
}
