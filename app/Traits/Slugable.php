<?php 

namespace RMF\Traits;

use Illuminate\Support\Str;

trait Slugable {

	public function slugEnd(){
		return '';
	}

	public function slug(){
    	return $this->id . '-' . str_slug($this->slugEnd());
    }

	public static function fromSlug($slug) {        
        $id = str_contains($slug,'-') ? explode('-', $slug)[0] : $slug;
        return static::where('id', $id);
    }

    public static function findFromSlug($slug) {
        return static::fromSlug($slug)->firstOrFail();
    }

    public static function findBySlug($slug) {
        return static::fromSlug($slug)->firstOrFail();
    }
}

