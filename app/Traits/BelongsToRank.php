<?php 

namespace RMF\Traits;

use RMF\Models\UserGroup;

trait BelongsToRank {
	public function ranks(){
        return $this->belongsToMany(UserGroup::class)->withTimestamps()->withPivot('active')->orderBy('priority', 'DESC');
    }

    public function activeGroups(){
        return $this->ranks()->wherePivot('active', 1);
    }

    public function rank(){
        $res = $this->activeGroups->first();
        if(!$res){
            $this->assignRank('null');
            return 'null';
        }
        return $res->name;
    }

    public function assignRank($rank){
        if(! $rank instanceof UserGroup)
            $rank = UserGroup::findByName($rank);

        $this->ranks()->attach($rank);
    }

    public function removeRank($rank, $perm = true){
        if(! $rank instanceof UserGroup)
            $rank = UserGroup::findByName($rank);

        if($this->ranks()->where('id', $rank->id)->count() == 0)
            return true;

        if($perm){            
            $this->ranks()->detach($rank);
            return $this->ranks()->where('id', $rank->id)->count() == 0;
        }

        DB::table('user_user_group')
            ->where('user_id', $this->id)
            ->where('user_group_id', $rank->id)
            ->update([
                'active' => 0,
                'updated_at' => Carbon::now()->timestamp,
            ]);
        return ! $this->isAMemberOf($rank);        
    }

    public function isAMemberOf($rank){
        if(!$rank instanceof UserGroup)
            $rank = UserGroup::findByName($rank);
        
        if(!$rank)
            return false;
        
        return $this->activeGroups()->where('id', $rank->id)->count() > 0;
    }
}
