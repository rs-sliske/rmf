<?php 

namespace RMF\Traits;

use RMF\Models\Permission;

trait Permissible {

    public function permissions(){
        return $this->morphToMany(Permission::class, 'authorizable', 'authorizable_permissions')->withTimestamps()->withPivot('permission_value');
    }

    public function checkPermission($permission, $minmax = 'max'){
        if(! $permission instanceof Permission)
            $permission = Permission::findByName($permission);
        
        $failResult = [
            'exists' => false,
            'on' => null,
            'value' => null,
            'model' => null,
        ];

        if(!$permission)
            return $failResult;

        $u = $this->permissions()->where('permission_id', $permission->id)->orderBy('pivot_permission_value', ($minmax == 'min' ? 'ASC' : 'DESC') )->first();
        if($u){
            return [
                'exists' => true,
                'on' => static::class,
                'value' => $u->pivot->permission_value,
                'model' => $u,
            ];
        }
    
        return $failResult;
    }

}

