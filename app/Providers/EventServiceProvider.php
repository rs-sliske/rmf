<?php

namespace RMF\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'RMF\Events\UserRegistered' => [
            'RMF\Listeners\SendWelcomeEmail',
        ],
        'RMF\Events\EmailAddressAdded' => [
            'RMF\Listeners\VerifyEmail',
        ],
        'RMF\Events\PasswordChanged' => [
            'RMF\Listeners\SendPasswordChangedWarningEmail',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
