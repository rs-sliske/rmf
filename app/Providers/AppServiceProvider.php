<?php

namespace RMF\Providers;

use Illuminate\Support\ServiceProvider;
use BBCode;
use URL;

use Carbon\Carbon;

use RMF\Models\Topic;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        BBCode::setParser('quote', '/\[quote\=([0-9]+)\](.+?)\[\/quote\]/s', '<quote post="$1">$2</quote>', '$2');

        URL::macro('toRoute', function($route, $parameters, $absolute = false){
            return $this->toRoute($route, $parameters, $absolute);
        });

        Topic::creating(function($topic){
            $topic->view_count = 0;
            $topic->bumped_at = Carbon::now();
            $topic->changed_at = Carbon::now();
            $topic->section_id = 0;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }

    }
}
