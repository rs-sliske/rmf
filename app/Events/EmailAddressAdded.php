<?php

namespace RMF\Events;

use RMF\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use RMF\Models\EmailAddress;

class EmailAddressAdded extends Event
{
    use SerializesModels;

    private $email;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EmailAddress $email)
    {
        $this->email = $email;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    public function email(){
        return $this->email;
    }
}
