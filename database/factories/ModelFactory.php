<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(RMF\Models\User::class, function (Faker\Generator $faker) {
	$username = $faker->userName;
    return [
        'email' => $faker->safeEmail,
        'username' => $username,
        'display_name' => $username,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(RMF\Models\Permission::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->word,
	];
});

$factory->define(RMF\Models\Topic::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'section_id' => $faker->numberBetween(1,20),        
    ];
});

$factory->define(RMF\Models\Post::class, function (Faker\Generator $faker) {
    return [
        'author_id' => $faker->numberBetween(1,10),
        'topic_id' => $faker->numberBetween(1,10), 
        'content' => $faker->paragraph,       
    ];
});

