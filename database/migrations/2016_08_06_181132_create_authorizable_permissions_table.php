<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorizablePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorizable_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('authorizable_type');
            $table->integer('authorizable_id')->unsigned();
            $table->integer('permission_id')->unsigned(); 
            $table->integer('permission_value')->nullable()->default(0);

            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('authorizable_permissions');
    }
}
