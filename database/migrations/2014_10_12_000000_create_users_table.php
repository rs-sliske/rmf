<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('display_name')->unique();
            $table->string('email')->unique()->nullable();
            $table->string('password');
            $table->string('google2fa_secret')->nullable();
            $table->boolean('google2fa_confirmed')->default(false);
            $table->string('magic_login_key')->nullable();
            $table->timestamp('magic_login_expires_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
