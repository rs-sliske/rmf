<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = RMF\Models\User::register(['username' => 'sliske', 'email' => 'admin@sliske.uk', 'password' => 'password']);
        $user->assignRank(RMF\Models\UserGroup::make('admin'));
        RMoore\SocialiteExtension\Models\SocialSite::forceCreate(
            [
                'name' => 'Facebook', 
                'class' => 'facebook', 
                'button' => 'facebook', 
                'app_id' => '1595565350738057', 
                'app_secret' => '221c738d390559d70117b2c9410b4e54'
            ]);
        RMoore\SocialiteExtension\Models\SocialSite::forceCreate(
            [
                'name' => 'Twitter', 
                'class' => 'twitter', 
                'button' => 'twitter', 
                'app_id' => '69VE5tc4XhjbR2CtlsH2icTuG',
                'app_secret'=>'RzdinA4nQEtS6Np7ZilWPOx7owEvwETclcI1lDldTExtf6I9Az'
            ]);
        RMoore\SocialiteExtension\Models\SocialSite::forceCreate(
            [
                'name' => 'GitHub', 
                'class' => 'github', 
                'button' => 'github', 
                'app_id' => 'cc3f5df13e67ccbe3f73', 
                'app_secret' => 'de8a6584117da4ac60ec856b290f6042a043a1e1'
            ]);
        factory(\RMF\Models\User::class, 9)->create();
        factory(\RMF\Models\Topic::class, 10)->create();
        factory(\RMF\Models\Post::class, 50)->create();

    }
}
