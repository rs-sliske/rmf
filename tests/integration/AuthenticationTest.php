<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use RMF\Models\User;
use RMF\Models\UserGroup;
use RMF\Models\Permission;

class AuthenticationTest extends TestCase
{
	use DatabaseMigrations, MailTracking;

    private function registerUser(array $args = []){
    	return User::register($this->getFields($args));
    }

    private function getFields(array $args = []){
    	return array_merge([
    		'username' => $this->fake->userName,
    		'password' => 'password',
    		'email' => $this->fake->safeEmail,
    	], $args);
    }


    /** @test */
    public function it_can_register_on_backend(){
    	$this->registerUser(['email' => 'test@test.dev']);
    	
    	$this->seeEmailsWereSent(1)
    		->seeEmailSubjectContains('Welcome')
    		->seeEmailTo('test@test.dev');
    }

    /** @test */
    public function it_can_have_a_permission(){
    	$user = $this->registerUser();
    	$permission = factory(Permission::class)->create();

    	$this->assertFalse($user->checkPermission($permission, 'max', false)['exists']);

    	$user->permissions()->attach($permission);

    	$this->assertTrue($user->checkPermission($permission, 'max', false)['exists']);
    }

    /** @test */
    public function it_can_be_found_by_name(){
    	$user = $this->registerUser(['username' => 'testing']);

    	$this->assertEquals($user->id, User::findByName('testing')->id);
    }

    /** @test */
    public function it_can_belong_to_a_group(){
    	$user = $this->registerUser();
    	$group = UserGroup::make('testing');

    	$this->assertFalse($user->isAMemberOf($group));

    	$user->ranks()->attach($group);
    	$user = $user->fresh();

    	$this->assertTrue($user->isAMemberOf($group));
    	$this->assertTrue($user->isAMemberOf($group->name));
    }

	/** @test */
	public function it_can_have_permissions_through_its_groups(){
		$user = $this->registerUser();
		$group = UserGroup::make('test');
		$permission = factory(Permission::class)->create();

		$user->ranks()->attach($group);
		$group->permissions()->attach($permission);

		$this->assertTrue($user->checkPermission($permission)['exists']);
	}

	/** @test */
	public function it_can_change_display_name(){
		$user = $this->registerUser(['username' => 'name']);

		$this->assertEquals($user->username, 'name');
		$this->assertEquals($user->name(), 'name');

		$user->changeName('new name');
		$user = $user->fresh();

		$this->assertEquals($user->username, 'name');
		$this->assertEquals($user->name(), 'new name');
	}

	/** @test */
	public function it_can_change_password(){
		$user = $this->registerUser(['password' => 'password']);

		$this->assertTrue(Hash::check('password', $user->password));

		$user->changePassword('new password');
		$user = $user->fresh();

		$this->assertFalse(Hash::check('password', $user->password));
		$this->assertTrue(Hash::check('new password', $user->password));
	}

	/** @test */
	public function it_sends_an_email_when_our_password_gets_changed(){
		$user = $this->registerUser(['password' => 'password', 'email' => 'test@test.com']);

		$this->seeEmailsWereSent(1);

		$user->changePassword('new password');

		$this->seeEmailsWereSent(2)
				->seeEmailSubjectIs('Your password has been changed')
				->seeEmailTo('test@test.com');
	}

	/** @test */
	public function it_has_a_rank_of_validating_when_it_is_registered(){
		UserGroup::make('validating');
		$user = $this->registerUser();

		$this->assertEquals('validating', $user->rank());
	}

	/** @test */
	public function it_has_a_rank(){
		$user = $this->registerUser();
		
		$user->assignRank(UserGroup::make('admin', 10));

		$this->assertEquals('admin', $user->rank());
	}

	/** @test */
	public function it_can_remove_a_rank(){
		$user = $this->registerUser();
		$rank = UserGroup::make('admin');

		$user->assignRank($rank);
		$this->assertEquals('admin', $user->rank());

		$user->removeRank('admin');
		$this->assertEquals($user->ranks()->where('name', 'admin')->count(), 0);
	}

	/** @test */
	public function it_has_email_addresses(){
		$user = $this->registerUser(['email' => 'test@test.dev']);

		$this->assertEquals($user->emails[0]->email, 'test@test.dev');
	}

	/** 
		@test 
		@group site
	*/
	public function it_can_register_through_site(){
		$fields = $this->getFields();

		$this->visit('register')
			->type($fields['username'], 'username')
			->type($fields['email'], 'email')
			->type($fields['password'], 'password')
			->type($fields['password'],'password_confirmation')
			->press('register');
		
		$this->assertResponseOk()
			->seeEmailWasSent();

		$user = User::findByName($fields['username']);

		$this->assertNotNull($user);

		$this->assertTrue($user->exists());
		$this->assertEquals($user->emails[0]->email, $fields['email']);
		$this->assertEquals($user->name(), $fields['username']);
		$this->assertTrue(Hash::check($fields['password'], $user->password));
	}

	/** 
		@test 
		@group site
	*/
	public function it_can_sign_in_to_site_with_username_and_password(){
		$user = $this->registerUser(['password' => 'password123']);

		$this->visit('/')
			->click('Login')
			->seePageIs('login')
			->type($user->username, 'username')	
			->type('password123', 'password')
			->press('login');

		$this->assertResponseOk();

		$this->seePageIs('/')
			->seeInElement('#navbar-username', $user->name())
			->assertEquals(auth()->id(), $user->id);
	}

}
