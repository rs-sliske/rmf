<?php 

use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ApiTester extends TestCase
{
	use DatabaseMigrations;
	
	protected $fake;
	protected $times = 1;

	function __construct()
	{
		$this->fake = Faker::create();
	}

	protected function times($count){
		$this->times = $count;
		return $this;
	}
}