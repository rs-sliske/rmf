var elixir = require('laravel-elixir');

require('laravel-elixir-vue');

elixir(function(mix) {
    mix.sass('app.scss');
    mix.sass('nojs.scss');
    mix.webpack('main.js');

    mix.version([
    	'public/css/app.css', 	
    	'public/css/nojs.css', 
    	'public/js/main.js', 
    ]);
});
