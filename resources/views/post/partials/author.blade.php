<img src="{{ $author->avatar() }}" class="post-avatar-img img-responsive">

<div class="author-details">	

	<h3>
		{{ $author->rank() }}
	</h3>
	<p>
		{{ $author->posts->count() }} post{{ $author->posts->count() == 1 ? '' : 's' }}
	</p>
	<div>
		@if(0)
		@foreach($author->pips() as $pip)
			@include('pips/'.$pip['name'].'.blade.php')
		@endforeach
		@endif
	</div>
	
</div>
