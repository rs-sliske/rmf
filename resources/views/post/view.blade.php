<post 
	:author="{{ $post->author->vueData() }}" 
	:id="{{ $post->id }}" 
	:time="{{ $post->created_at->timestamp }}"
	content="{{ $post->content }}"
>   
	<span slot="author">{{ $post->author->display_name }}</span>
    <div slot="content">
		{!! BBCode::parse(htmlentities($post->content)) !!}
    </div> 
    <div slot="footer">
    
    </div>
</post>




