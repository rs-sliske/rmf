<article class="post">
	<aside class="post-author">
		<div class="post-header">
			<span class="display-name display-name__{{ $post->author->rank() }}">{{ $post->author->display_name }}</span>
		</div>

		@include('post.partials.author',['author' => $post->author])
		
	</aside>

	<div class="post-content">
		<div class="post-heading post-header">
			<timestamp :timestamp="{{ $post->created_at->timestamp }}" tooltip>{{ $post->created_at->diffForHumans() }}</timestamp>
		</div>
		<div class="post-content-text">
			{!! BBCode::parse(htmlentities($post->content)) !!}
		</div>

		<div class="post-footer">	
			<form class="form-horizontal post-action-button">
				<button type="submit" class="btn btn-default btn-sm btn__quote">Quote</button>
			</form>			
			
			@if(Auth::id() == $post->author_id)
				<form class="form-horizontal post-action-button">
					<button class="btn btn-default btn-sm btn__delete">Delete</button>
				</form>
			@endif
		</div>
	</div>
</article>