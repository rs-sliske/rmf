@extends('layouts.app')
@section('content')

<div class="container">
	<div class="panel  panel-default">
		<div class="panel-heading">Editing User : {{ $user->name() }}</div>
		<div class="panel-body">
		{!! Form::model($user, array('url' => route('user.update', ['user'=>$user->slug()]), 'method' => 'put')) !!}

		{!! Form::bsText('display_name') !!}

		{!! Form::bsSubmit('Update') !!}

		{!! Form::close() !!}
	</div></div>
</div>


@endsection