<div class="form-group">
	{{ Form::button($name, array_merge(['type' => 'submit', 'class' => 'btn'], $attributes)) }}
</div>