@extends('layouts.app')
@section('content')

<div class="container">
<div class="thread-title-well">
<h1 class="thread-title">{{ $topic->title }}</h1>
</div>
<br>
<div class="post-list-well">
	<div class="topic-reply-count">
		<div>
			<span>This topic has been viewed {{ $topic->viewCount() }} {{ $topic->viewCount() == 1 ? 'time' : 'times' }} and has {{ $topic->posts->count() - 1 }} replies.</span>
		</div>
		<div>
			@if(Auth::id() == $topic->posts->first()->author_id)
				<form class="form-horizontal post-action-button" action="{{route('community.topic.bump', ['topic'=>$topic->slug()]) }}" method="post">
					{{ csrf_field() }}
					<button class="btn btn-default btn-sm btn__bump">Bump</button>
				</form>
			@endif
		</div>
		
	</div>
	<div class="">
		@foreach($topic->posts as $post)
			@if($useJS)	
				@include('post.view')
			@else		
				@include('post.nojs')			
			@endif
		@endforeach
	</div>
	@if(Auth::check())
		<br>
		@include('topic.partials.reply')
	@endif

</div>
</div>

@endsection