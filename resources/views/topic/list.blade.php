@extends('layouts.app')
@section('content')

<div class="container">
	<div class="well">
		<ul style="padding:0px;">
			@foreach($topics as $topic)
				@if($topic->posts->count() > 0)					
					<li class="topic-list__link">
						<div class="read-circle {{ $topic->hasBeenRead() ? 'read' : 'unread'}}"></div>

						<div class="topic-list__title">
							<a href="{{ route('topic.show', ['topic' => $topic->slug()]) }}" class="block">
								{{ $topic->title }}										
							</a>

							<span>Posted {{ $topic->created_at->diffForHumans() }}</span>
						</div>

						<div class="topic-list__stats">
							<span>
								{{ $topic->posts->count() - 1 }} {{ $topic->posts->count() == 2 ? 'Reply' : 'Replies'}}
							</span>

							<span>
								{{ $topic->viewCount() }} Views
							</span>
						</div>

						<div class="topic-list__last-poster">
							<img src="{{ $topic->lastPoster()->avatar() }}" 
								style="display: inline-table;" height="50" width="50"
							>

							<div class="topic-list__last-poster__info">
								<a 
									class="block display-name display-name__{{ $topic->lastPoster()->rank() }}" 
									style="text-align:left;" 
									href="{{ route('user.show', ['user' => $topic->lastPoster()->slug()]) }}"
								>
									<sized-text :width="150" :max-size="1">{{ $topic->lastPoster()->name() }}</sized-text>
								</a>
								<span>
									{{ $topic->posts->last()->created_at->diffForHumans() }}
								</span>
							</div>
						</div>
					</li>
				@endif
			@endforeach
		</ul>
	</div>
</div>


@endsection