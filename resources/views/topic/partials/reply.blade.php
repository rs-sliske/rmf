<form method="POST" action="{{ route('post.store') }}">
	{{ csrf_field() }}

	<input type="hidden" name="topic" value="{{ $topic->id }}">
	
	<div class="form-group">
		<textarea name="content" class="form-control" v-ckeditor></textarea>
	</div>

	<button type="submit" class="btn btn-primary">
        Reply
    </button>
	
	
</form>