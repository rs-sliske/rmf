@extends('layouts.app')
@section('content')

<div class="container">

<form method="post" action="{{ route('community.topic.store') }}" class="form">
	{{ csrf_field() }}

	<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="title" class="col-md-4 control-label">Title</label>

        <div class="col-md-6">
            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">

            @if ($errors->has('title'))
                <span class="help-block">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
        <label for="content" class="col-md-4 control-label">Content</label>

        <div class="col-md-6">
            <textarea id="content" type="textarea" rows="10" class="form-control" name="content" value="{{ old('content') }}" v-ckeditor></textarea>

            @if ($errors->has('content'))
                <span class="help-block">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Submit
            </button>
        </div>
    </div>
	

</form>



</div>


@endsection