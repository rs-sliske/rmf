<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>RMF</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    @if(Request::exists('nojs'))
        <link href="{{ elixir('css/nojs.css') }}" rel="stylesheet">
    @endif

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    
</head>
<body id="app-layout">
    @include('layouts.partials.navigation')

    <div class="container-fluid" id="s-alerts">
        @include('layouts.partials.alerts')
    </div>

    @yield('content')
    
    <script src="http://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>    
    <script>
        <!--
        window.currentUser = {
            id: {{ Auth::id() ?: -1 }},
            name: "{{ Auth::check() ? Auth::user()->display_name : '' }}"
        }
        //-->
    </script>
    @if(! Request::exists('nojs'))
        <script src="{{ elixir('js/main.js') }}"></script>
    @endif
</body>
</html>
