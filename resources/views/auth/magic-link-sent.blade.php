@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login as {{ $user->name() }}</div>
                <div class="panel-body">
                    <div class="center-block" style="text-align:center;">
                        <h2>Your Magic Login Link has been sent.</h2>
                        <h3>Please check your emails</h3>
                        <a href="{{ route('auth.magic.send' , ['user' => $user->username]) }}" class="btn btn-md btn-primary" style="margin:20px;">Click here to resend link</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
