@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/2fa') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('secret') ? ' has-error' : '' }}">
                            <label for="secret" class="col-md-4 control-label">Code</label>

                            <div class="col-md-6">
                                <input id="secret" type="text" class="form-control" name="secret">

                                @if ($errors->has('secret'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('secret') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" id="login" name="login">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
