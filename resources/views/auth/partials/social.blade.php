<div class="row">
    @foreach((isset($sites) ? $sites : RMoore\SocialiteExtension\Models\SocialSite::active()->get()) as $site)
        <div class="col-md-6 col-lg-4" style="margin-bottom:10px;">
            <a 
                class="btn btn-block btn-social btn-{{ $site->class }}"
                href="{{ route('auth.social.redirect', ['provider' => $site->class ]) }}"
            >
                <span class="fa fa-{{ $site->button }}"></span>
                {{ $message }} with {{ $site->name }}
            </a>    
        </div>
    @endforeach
</div>