require('./bootstrap');
window.VueStrap = require('vue-strap/dist/vue-strap.min.js');


// window.CKEDITOR = require('ckeditor');

import Store from './store';

import ckedit from './directives/ckeditor'

Vue.directive('ckeditor', ckedit);

Vue.component('quote', require('./components/Quote.vue'));

var vm = new Vue({
	el:'body',

	data:{
		reply:''
	},
	computed:{
		
	},
	components:{
		Post:require('./components/Post.vue'),
		UserName:require('./components/DisplayName.vue'),
		Timestamp:require('./components/Timestamp.vue'),
		SizedText:require('./components/Text.vue'),

		PassportClients:require('./components/passport/Clients.vue'),
		PassportAuthorizedClients:require('./components/passport/AuthorizedClients.vue'),
		PassportPersonalAccessTokens:require('./components/passport/PersonalAccessTokens.vue'),
	},
	events:{
		quotedPost(post){
			// console.log('quoted post : '+post.id);
			this.$broadcast('quotedPost', post);
		}
	},
	mixins:[
		Store
	],
	ready(){
		this.store.users = {};
	},
});



