export default {
	params:['v-model'],
	data(){
		return {
			editor:{},
		}
	},
	bind(){
		this.editor = CKEDITOR.replace(this.el,{
			extraPlugins:'bbcode',
		});
		// var model = this.params['v-model'];
		// if(model){
		// 	console.log('binding to - '+model);
		// 	editor.on('change', function(e){
		// 		vm[model] = e.editor.getData();
		// 	});
		// }
		// editor.on('change', function(e){
		// 	console.log( e.editor.getData());
		// });
	},
	events:{
		quotedPost(post){
			console.log('quoted post : '+post.id);
			var data = this.editor.getData();
			data += '[quote=' + post.id + ']' + post.content + '[/quote]';
			this.editor.setData(data);
		}
	}
}