const store = {
	posts:{},
	currentUser:{
		id:-1,
		name:'',
	},

	resources:{},
};

export default {
	data(){
		return {
			store:store,
		};
	},

	methods:{
		getPost(id){
			if(this.store.posts[id])
				return this.store.posts[id];

			if(this.store.resources.post)
				this.store.resources.post.get({id:id}).then((response) => {
					this.$set('store.posts['+id+']', response.data);

				});

			return {
				created_at:0,
			};
		}
	},

	ready(){
		var v = window.currentUser;
		if(v)
			this.store.currentUser = v;
		this.$set('store.resources.post', this.$resource('/api/v1/community/post{/id}'))
		
	},


}